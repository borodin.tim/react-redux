import { useSelector, useDispatch } from 'react-redux';

import { counterActions } from '../store/counter-slice';
import classes from './Counter.module.css';

const Counter = () => {
  const dispatch = useDispatch();
  const counter = useSelector(state => state.counter.counter);
  const showValue = useSelector(state => state.counter.show);

  const incrementHandler = () => {
    // dispatch({ type: 'inc' });
    dispatch(counterActions.increment());
  }

  const decrementHandler = () => {
    // dispatch({ type: 'dec' });
    dispatch(counterActions.decrement());
  }

  const increaseHandler = () => {
    // dispatch({ type: 'increase', value: 5 }); // payload instead of value when using slices
    dispatch(counterActions.increase(5));
  }

  const toggleCounterHandler = () => {
    // dispatch({ type: 'toggle' });
    dispatch(counterActions.toggle());
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter using Hooks</h1>
      {showValue && <div className={classes.value}>{counter}</div>}
      <div>
        <button onClick={incrementHandler}>Increment</button>
        <button onClick={increaseHandler}>Increase by 5</button>
        <button onClick={decrementHandler}>Decrement</button>
      </div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
