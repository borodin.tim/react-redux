import { createSlice } from '@reduxjs/toolkit';

const initialCounterState = { counter: 0, show: true };
const counterSlice = createSlice({
    name: 'counter',
    initialState: initialCounterState,
    reducers: {
        increment(state) {
            state.counter++;
        },
        decrement(state) {
            state.counter--;
        },
        increase(state, action) {
            state.counter += action.payload;
        },
        toggle(state) {
            state.show = !state.show
        }
    }
});

export const counterActions = counterSlice.actions;

export default counterSlice.reducer;

// const counterReducer = (state = initialState, action) => {
//     switch (action.type) {
//         case 'inc':
//             return {
//                 ...state,
//                 counter: state.counter + 1
//             };
//         case 'dec':
//             return {
//                 ...state,
//                 counter: state.counter - 1
//             };
//         case 'increase':
//             return {
//                 ...state,
//                 counter: state.counter + action.value
//             };
//         case 'toggle':
//             return {
//                 ...state,
//                 show: !state.show
//             };
//         default:
//             return state;
//     }
// }